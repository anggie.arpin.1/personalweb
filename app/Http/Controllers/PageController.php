<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //dibuat variabel static agar hanya sekali mendeklarasikan isinya
    private static $konten = [
        [
            "judul" => "Semesta Kita",
            "id" => "1",
            "img" => "1.jpg",
            "link" => "https://semesta.id/",
            "short" => "Sistem Manajemen Sampah Terpadu",
            "deskripsi" => "Sistem Manajemen Sampah Terpadu yang kemudian disebut dengan SEMESTA KITA adalah sebuah sistem informasi manajemen yang membantu program bank sampah untuk dapat melakukan kegiatan administrasi pengelolaan sampah menjadi lebih efektif dan efisien. SEMESTA KITA dapat melakukan manajemen data anggota, pembukuan, data keluar-masuk, manajemen data sampah, promosi produk, serta manajemen iuran, sehingga pengelola serta anggota bank sampah dapat melakukan aktivitas manajemen dengan lebih mudah.",
            "author" => "Anggie Arpin",
            "date" => "February 30, 2020"
        ],
        [
            "judul" => "SSO Informatics",
            "id" => "2",
            "img" => "2.jpg",
            "link" => "https://if.undiksha.ac.id/",
            "short" => "Single Sign On Informatics HMJ TI Undiksha",
            "deskripsi" => "SSO Informatics adalah Sistem Yang dimiliki oleh HMJ TI Undiksha.",
            "author" => "Anggie Arpin",
            "date" => "February 30, 2020"
        ],
        [
            "judul" => "Prototype Application",
            "id" => "3",
            "img" => "3.jpg",
            "short" => "Prototype Aplikasi Rudaya ~ Connect The Art",
            "link" => "https://www.figma.com/file/xkeeLL6yJm8TxqChOB8sQM/New-Rudaya?node-id=7%3A2",
            "deskripsi" => "Ini adalah Prototype Aplikasi Rudaya ~ Connect The Art.",
            "author" => "Anggie Arpin",
            "date" => "February 30, 2020"
        ]
        
    ];

    //ke landing
    public function index(){
        return view('index',[
            "header" => "My Profile",
            "konten" => PageController::$konten
        ]);
    }

    //tampil semua produk
    public function allproduct(){
        return view('Portofolio.portofolio',[
            "header" => "My Product",
            "konten" => PageController::$konten
        ]);
    }

    //ke portofolio 1
    public function page1(){
        return view('Portofolio.portofolio1',[
            "header" => "My Product",
            "konten" => PageController::$konten[0]
        ]);
    }

    //ke portofolio 2
    public function page2(){
        return view('Portofolio.portofolio2',[
            "header" => "My Product",
            "konten" => PageController::$konten[1]
        ]);
    }

    //ke portofolio 1
    public function page3(){
        return view('Portofolio.portofolio3',[
            "header" => "My Product",
            "konten" => PageController::$konten[2]
        ]);
    }
}

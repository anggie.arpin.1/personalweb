<?php

use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', [PageController::class, 'index'])->name('landing');
Route::get('/', [PageController::class, 'index']);

Route::get('/portofolio',[PageController::class, 'allproduct']);

Route::get('/portofolio1',[PageController::class, 'page1']);

Route::get('/portofolio2', [PageController::class, 'page2']);

Route::get('/portofolio3', [PageController::class, 'page3']);

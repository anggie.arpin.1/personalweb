@extends('layout.main')

@section('content')
    <div class="caption-header text-center wow zoomInDown">
        <h1 class="fw-normal">My Product</h1>
        <div class="foot">ooo</div></div> 
    </div>

    <div class="vg-page page-about center">
        <div class="container">
            <div class="d-flex justify-content-center">
                <div class="col-md-9">
                    <div class="d-flex justify-content-center">
                        <img src="assets/img/{{$konten["img"]}}" class="img-responsive" alt="images">
                    </div>
                    <div class="container">
                        <h3>{{$konten["judul"]}}</h3>
                        <h6>Author : {{$konten["author"]}}</h6>
                        <h6>Date : {{$konten["date"]}}</h6>
                        <p>{{$konten["deskripsi"]}}</p>
                        <a href="{{$konten["link"]}}">Mari Kunjungi Project Ini >></a>
                        <div class="button mb-3">
                            <a class="btn btn-primary" href="/portofolio1" role="button">< Back</a>
                            <a class="btn btn-primary" href="/portofolio3" role="button">Next ></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
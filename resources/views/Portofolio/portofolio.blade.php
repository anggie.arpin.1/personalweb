@extends('layout.main')

@section('content')
    <div class="caption-header text-center wow zoomInDown">
        <h1 class="fw-normal">My Product</h1>
        <div class="foot">ooo</div></div> 
    </div>

    <div class="vg-page page-blog" id="blog">
        <div class="row post-grid">
            @foreach ($konten as $isi)
            <div class="col-md-6 col-lg-4 wow fadeInUp">
                <div class="card">
                <div class="img-place">
                    <img src="assets/img/{{$isi["img"]}}" alt="img">
                </div>
                <div class="caption">
                    <a href="{{$isi['link']}}" class="post-category">{{$isi["judul"]}}</a>
                    <a href="/portofolio{{$isi["id"]}}" class="post-title">{{$isi["short"]}}</a>
                    <span class="post-date"><span class="sr-only">Published on</span> {{$isi["date"]}}</span>
                </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>   
@endsection
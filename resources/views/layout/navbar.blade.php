<!-- Navbar -->
<div class="navbar navbar-expand-lg navbar-dark sticky" data-offset="500">
    <div class="container">
        <a href="/" class="navbar-brand">{{$header}}</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#main-navbar" aria-expanded="true">
        <span class="ti-menu"></span>
        </button>
        <div class="collapse navbar-collapse" id="main-navbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a href="/" class="nav-link {{ ($header === "My Profile") ? 'active' : ''}}" data-animate="scrolling">Home</a>
            </li>
            <li class="nav-item">
            <a href="/portofolio" class="nav-link {{ ($header === "My Product") ? 'active' : ''}}" data-animate="scrolling">Product</a>
            </li>
        </ul>
        </div>
    </div>
</div> <!-- End Navbar -->
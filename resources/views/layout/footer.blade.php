<!-- Footer -->
<div class="vg-footer">
    <h1 class="text-center">Virtual Folio</h1>
    <div class="container" >
      <div class="row">
        <div class="col-lg-4 py-3">
          <div class="footer-info">
            <p>Where to find me</p>
            <hr class="divider">
            <p class="fs-large fg-white">Banjar Tegal, Kubutambahan Village, Buleleng 81172 Indonesia</p>
          </div>
        </div>
        <div class="col-md-6 col-lg-3 py-3">
          <div class="float-lg-right">
            <p>Contact me</p>
            <hr class="divider">
            <ul class="list-unstyled">
              <li>anggie.arpin.1@gmail.com</li>
              <li><a href="https://api.whatsapp.com/send?phone=6282237256677">+6282237256677</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-6 col-lg-3 py-3">
          <div class="float-lg-right">
            <p>Follow me</p>
            <hr class="divider">
            <ul class="list-unstyled">
              <li><a href="https://www.instagram.com/anggie.arpin/" target="blank">Instagram</a></li>
              <li><a href="https://www.facebook.com/anggie.arpin.9/" target="blank">Facebook</a></li>
              <li><a href="https://www.linkedin.com/in/i-gede-anggie-suardika-arpin-53802b1a2/" target="blank">LinkedIn</a></li>
              <li><a href="https://www.youtube.com/channel/UCp7c0cAkSYwmvBEUd969yew" target="blank">Youtube</a></li>
            </ul>
          </div>
        </div>
      </div>
        <div class="col-12">
          <p class="text-center mb-0 mt-4">Copyright &copy;2021.</p>
        </div>
      </div>
    </div>
  </div> <!-- End footer -->
  
  <script src="assets/js/jquery-3.5.1.min.js"></script>
    
  <script src="assets/js/bootstrap.bundle.min.js"></script>
    
  <script src="assets/vendor/owl-carousel/owl.carousel.min.js"></script>
    
  <script src="assets/vendor/perfect-scrollbar/js/perfect-scrollbar.js"></script>
    
  <script src="assets/vendor/isotope/isotope.pkgd.min.js"></script>
    
  <script src="assets/vendor/nice-select/js/jquery.nice-select.min.js"></script>
    
  <script src="assets/vendor/fancybox/js/jquery.fancybox.min.js"></script>

  <script src="assets/vendor/wow/wow.min.js"></script>

  <script src="assets/vendor/animateNumber/jquery.animateNumber.min.js"></script>

  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <script src="assets/js/google-maps.js"></script>
    
  <script src="assets/js/topbar-virtual.js"></script>

  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIA_zqjFMsJM_sxP9-6Pde5vVCTyJmUHM&callback=initMap"></script>